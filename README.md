# prettier-config

## Installation

With npm:

```shell
npm install --save-dev prettier @tkhiking/prettier-config
```

With npm:

```shell
yarn add --dev prettier @tkhiking/prettier-config
```

## Usage

Edit `package.json`:

```json
{
  "name": "your-cool-library",
  "version": "0.1.0",
  "prettier": "@tkhiking/prettier-config"
}
```

Or `prettier.config.js`:

```javascript
module.exports = '@tkhiking/prettier-config';
```

## License

This project is licensed under the terms of the MIT license.

Copyright (c) 2020 tkhiking.
